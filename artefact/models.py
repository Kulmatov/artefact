from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models import SET_NULL
from django.contrib.gis.geos import Point
from django.contrib.gis.db import models as goe_models

from django.utils.timezone import now


# Create your models here.
class User(AbstractUser):
    full_name = models.CharField(max_length=256)
    adress = models.CharField(max_length=255)
    # username = models.CharField(db_index=True, unique=True, max_length=255)
    email = models.EmailField(unique=True)
    sms_code = models.IntegerField(null=True)
    was_born = models.DateField(auto_now=True)
    sex = models.BooleanField(null=True)
    avatar = models.ImageField(upload_to="user_img/", null=True, blank=True)
    desc = models.CharField(max_length=255, default="", null=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    # def __str__(self):
    #     return self.username


class Subscription(models.Model):
    from_user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='from_user')
    to_user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='to_user')
    # fromUser = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    # toUser = models.ForeignKey(User, on_delete=models.CASCADE, null=True)


class Bloc(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=256, default="")
    media = models.FileField(upload_to="user_video/", null=True, blank=True)
    likee = models.BooleanField(null=True)
    point = goe_models.PointField(null=True, spatial_index=True, geography=True, blank=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=6, null=True, blank=True)
    longitude = models.DecimalField(max_digits=10, decimal_places=6, null=True, blank=True)

    class Meta:
        verbose_name_plural = "Bloc"
        # ordering = ['order']

    def save(self, *args, **kwargs):
        if self.point != None:
            self.latitude = self.point.y
            self.longitude = self.point.x

        elif self.longitude != None and self.latitude != None:
            self.point = Point(float(self.longitude), float(self.latitude), srid=4326)

        super(Bloc, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class Comment(models.Model):
    comment = models.TextField(default="")
    date_added = models.DateTimeField(auto_now_add=True)
    reply = models.ForeignKey('self', on_delete=SET_NULL, null=True, blank=True, related_name='reply_comment')
    bloc = models.ForeignKey(Bloc, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.user.username


class Like(models.Model):
    bloc = models.ForeignKey(Bloc, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, null=True, blank=True)
    likee = models.BooleanField(null=True, blank=True)


    # def __str__(self):
    #     return self.user.username


# class Save(models.Model):
#     bloc = models.ForeignKey(Bloc, on_delete=models.CASCADE, null=True)
#     user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)


class UserSave(models.Model):
    bloc = models.ForeignKey(Bloc, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.user.username


class Forvard_link(models.Model):
    bloc = models.ForeignKey(Bloc, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.user.username


# point = goe_models.PointField(null=True, spatial_index=True, geography=True, blank=True)


