from django.contrib import admin
from artefact.models import *
from django.contrib.gis.admin import OSMGeoAdmin

# Register your models here.
admin.site.register(User)
# admin.site.register(Bloc)
admin.site.register(Like)
# admin.site.register(Comment)
admin.site.register(UserSave)
admin.site.register(Forvard_link)
admin.site.register(Subscription)


class BlocAdmin(OSMGeoAdmin):
    default_lon = 7474999
    default_lat = 4931196
    default_zoom = 5

    class Meta:
        model = Bloc


admin.site.register(Bloc, BlocAdmin)


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):  # adminkada korinishi uchun
    readonly_fields = ('date_added',)
