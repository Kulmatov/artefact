from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIRequestFactory
from rest_framework import status
from rest_framework.test import APITestCase
from artefact.models import User


class ClinicTestCase(APITestCase):
    def test_get_list(self):
        # PGSq'da user creat qilyapti biz bergan login(admin) va parol(123) bilan
        user = User.objects.create_user('admin', '123')

        # yaratilgan user'ni login qilyapti
        self.assertTrue(self.client.force_authenticate(user))

        # shu silkabilan hamma comment'larni olyapti
        request = self.client.get("/api/v1/only_comment/")

        # test jonatgan javob bilan biz kutgan javob solishtirilayapti
        self.assertEqual(request.status_code, status.HTTP_200_OK)


