from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, mixins

from artefact.api.filter import CommentFilter
from artefact.api.serilizer import *
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.contrib.gis.db.models.functions import GeometryDistance
from django_filters import rest_framework as filters

from paginations import CommentListPagination


class UserViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (DjangoFilterBackend,)

    def get_queryset(self):
        return User.objects.filter(id=self.request.user.id)


class UserSaveViewSet(mixins.ListModelMixin,
                      mixins.DestroyModelMixin,
                      viewsets.GenericViewSet):
    queryset = UserSave.objects.all()
    serializer_class = UserSaveSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (DjangoFilterBackend,)


class UserBlocViewSet(mixins.ListModelMixin,
                      mixins.DestroyModelMixin,
                      viewsets.GenericViewSet):
    queryset = Bloc.objects.all()
    serializer_class = UserBlocSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (DjangoFilterBackend,)

    def get_queryset(self):
        print(self.request.user.id)
        return Bloc.objects.filter(user=self.request.user).all()


class BlocViewset(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet,
                  mixins.UpdateModelMixin):
    queryset = Bloc.objects.all()
    serializer_class = BlocSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (DjangoFilterBackend,)
    pagination_class = CommentListPagination

    def get_queryset(self):
        user = self.request.user
        request = self.request
        if self.action == "list":
            latitude1 = request.GET.get("latitude1")
            longitude1 = request.GET.get("longitude1")
            ref_location = Point(float(latitude1), float(longitude1), srid=4326)
            print(user.id)
            ids = list(Subscription.objects.filter(from_user=user).values_list("to_user_id", flat=True))
            print(ids)
            qs = Bloc.objects.filter(user__in=ids)
            qs = qs.annotate(distance=GeometryDistance("point", ref_location)).order_by(
                '-distance')  # (-)bu kichikdan kattaga qarab
            return qs
        return Bloc.objects.all()

    # def get_queryset(self):
    #     print(self.request.user.id)
    #     ids = list(Subscription.objects.filter(from_user=self.request.user).values_list("to_user_id", flat=True))
    #     print(ids)
    #     return Bloc.objects.filter(user__in=ids)


class CommentViewset(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet,
                     mixins.UpdateModelMixin):
    queryset = Comment.objects.all()
    pagination_class = CommentListPagination
    serializer_class = CommentSerializer1
    permission_classes = [IsAuthenticated]
    filter_backends = (DjangoFilterBackend,)
    filterset_class = CommentFilter

    def get_queryset(self):
        print(self.request.user.id)
        ids = list(Subscription.objects.filter(from_user=self.request.user).values_list("to_user_id", flat=True))
        print(ids)
        return Comment.objects.filter(user__in=ids)


class FromViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    queryset = Subscription.objects.all()
    serializer_class = FromSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (DjangoFilterBackend,)

    def get_queryset(self):
        print(self.request.user.id)
        return Subscription.objects.filter(from_user=self.request.user).all()


class ToViewSet(mixins.ListModelMixin,
                mixins.RetrieveModelMixin,
                viewsets.GenericViewSet):
    queryset = Subscription.objects.all()
    serializer_class = ToSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (DjangoFilterBackend,)

    def get_queryset(self):
        print(self.request.user.id)
        return Subscription.objects.filter(to_user=self.request.user).all()
