from rest_framework import status
from rest_framework_jwt.settings import api_settings
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.decorators import api_view, permission_classes
from artefact.api.serilizer import UserSerializer
from django.core.mail import EmailMessage
import random
from artefact.models import User

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER  # token uchun
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


def send_sms(mail, text):
    email = EmailMessage('Veri', text, to=[mail])
    email.send()


# User log in (avtorizatsiya 1-martta royxatdan otganda)
@api_view(['POST'])
@permission_classes([AllowAny])
def save_register(request):
    try:
        email = request.data.get('email')
        password = request.data.get('password')
        password2 = request.data.get('password2')

        user = User.objects.filter(email=email).first()
        if user or password != password2:  # agar user kiritgan email'ga o'xshagan email bolsa-
            res = {
                "status": 0,
                "error": "User alerdy exits or wrong password"  # -unda bunday user bor degan yozuv qaytaramiz
            }  # bolmasam-
            return Response(res)
        else:
            user = User.objects.create(  # -yangi user yaratamiz
                email=email
                # password=password,
            )
        user.set_password(password)  # password tekshirish (password=password)
        user.save()

        # token yasash
        payload = jwt_payload_handler(user)  # token uchun
        token = jwt_encode_handler(payload)
        res = {
            "status": 1,
            "msg": "Login",
            "user": UserSerializer(user, many=False).data,  # user ma'lumotlari qaytarilayapti
            "token": token
        }
        return Response(res)

    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }

    return Response(res)


# User sahifasiga kirayotganda
@api_view(["POST"])
@permission_classes([AllowAny])
def login_view(request):
    try:
        username = request.data['username']
        password = request.data['password']
        if not login_view:
            res = {
                'msg': 'Login empty',
                'status': 0,
            }
            return Response(res)
        user = User.objects.filter(username=username).first()
        if not user:
            user = User.objects.filter(email=username).first()
            if not user:
                res = {
                    'msg': 'email or password wrond',
                    'status': 0,
                }
                return Response(res)
        # user name va paroli biz kiritgan qiymatlarga teng bo'lsa login qilamiz (kirishga ruxsat berilsdi)
        if user and user.check_password(password):
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            res = {
                "status": 1,
                "msg": "Login",
                "user": UserSerializer(user, many=False).data,
                "token": token
            }
        else:
            res = {
                "status": 0,
                "error": "login or password error"
            }
        return Response(res)

    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }
    return Response(res)


# username tugilgan sana va jinsi yaratish
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def create_username(request):
    username = request.data.get('username')
    was_born = request.data.get('was_born')
    sex = request.data.get('sex')
    user = request.user
    if user:
        print(username, was_born)
        user.username = username
        user.was_born = was_born
        user.sex = sex
        user.save()
        res = {
            "status": 1,
            "error": "created successfully"
        }
    else:
        res = {
            "status": 0,
            "error": "error"
        }
    return Response(res)


# Profilni tiklash
@api_view(["POST"])
@permission_classes([AllowAny])
def forgot_password(request):
    try:
        username = request.data.get('username')
        user = User.objects.filter(email=username).first()
        if not user:
            user = User.objects.filter(username=username).first()
            if not user:
                res = {
                    'status': 0,
                    'msg': 'Can not authenticate with the given credentials or the account has been deactivated'
                }
                return Response(res, status=status.HTTP_403_FORBIDDEN)
        sms_code = random.randint(1000, 9999)
        user.sms_code = sms_code
        user.save()
        send_sms(user.email, "Ваш код подтверждения: " + str(sms_code))
        result = {
            'status': 1,
            'msg': 'Sms sended',
        }
        return Response(result, status=status.HTTP_200_OK)

    except KeyError:
        res = {
            'status': 0,
            'msg': 'Please set all reqiured fields'
        }
        return Response(res)


# user tomonidan code orqali tasdiqlash
@api_view(["POST"])
@permission_classes([AllowAny])
def accept_confirmation(request):
    try:
        sms_code = request.POST['sms_code']
        username = request.POST['username']
        user = User.objects.filter(username=username).first()

        if user and str(user.sms_code) == str(sms_code):
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            user.save()
            res = {
                "status": 1,
                "msg": "successful",
                "user": UserSerializer(user, many=False, context={"request": request}).data,
                "token": token
            }
        else:
            res = {
                "status": 0,
                "msg": "EROR",
            }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }
    return Response(res)


# yangi password kiritish
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def new_password(request):
    try:
        new_password = request.data['password']
        new_password2 = request.data['password2']
        user = request.user
        if user and new_password == new_password2:
            user.set_password(new_password)
            user.save()
            res = {
                "password changed successfully"
            }
        else:
            res = {
                "status": 0,
                "error": "wrong password"
            }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "error": "Key error"
        }

    return Response(res)
