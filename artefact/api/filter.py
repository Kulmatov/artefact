from django_filters import rest_framework as filters
from artefact.models import *


class CommentFilter(filters.FilterSet):
    bloc = filters.ModelChoiceFilter(queryset=Bloc.objects.all(), field_name='bloc')

    class Meta:
        model = Comment
        fields = ['bloc']


# class CommentLikeFilter(filters.FilterSet):  # Userga bog'liq bloclarni chiqaryabmiz
    # like = filters.ModelChoiceFilter(queryset=Like.objects.all(), field_name='like')
    #
    # class Meta:
    #     model = Like
    #     fields = ['like']
