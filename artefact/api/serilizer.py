from django.db.models import Avg, Sum, Count
from rest_framework import serializers
from artefact.models import *


class UserSerializer(serializers.ModelSerializer):
    sum_save = serializers.SerializerMethodField()
    like_avg = serializers.SerializerMethodField()
    link_sum = serializers.SerializerMethodField()
    comment_sum = serializers.SerializerMethodField()
    to_usercount = serializers.SerializerMethodField()
    from_usercount = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = "__all__"

    def get_like_avg(self, obj):
        return Like.objects.filter(user=obj).aggregate(Count('id'))

    def get_sum_save(self, obj):
        return UserSave.objects.filter(user=obj).aggregate(Count('id'))

    def get_link_sum(self, obj):
        return Forvard_link.objects.filter(user=obj).aggregate(Count('id'))

    def get_comment_sum(self, obj):
        return Comment.objects.filter(user=obj).aggregate(Count('id'))

    def get_to_usercount(self, obj):
        return Subscription.objects.filter(to_user=obj).count()  # подписчики

    def get_from_usercount(self, obj):
        return Subscription.objects.filter(from_user=obj).count()  # подписки


class UserSaveSerializer(serializers.ModelSerializer):
    bloc_save = serializers.SerializerMethodField()

    class Meta:
        model = UserSave
        fields = "__all__"

    def get_bloc_save(self, obj):
        # bloc = UserSave.objects.filter(user=obj).all()
        return BlocSerializer(obj.bloc, many=False, ).data


class UserBlocSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bloc
        fields = "__all__"


class BlocSerializer(serializers.ModelSerializer):
    like_avg = serializers.SerializerMethodField()
    comment_sum = serializers.SerializerMethodField()
    link_sum = serializers.SerializerMethodField()
    sum_save = serializers.SerializerMethodField()
    like = serializers.SerializerMethodField()

    class Meta:
        model = Bloc
        fields = "__all__"

    def get_user(self, obj):
        user = Bloc.objects.filter(user=obj).first()
        return UserSerializer(user, many=False, ).data

    def get_like_avg(self, obj):
        return Like.objects.filter(bloc=obj).aggregate(Count('bloc'))

    def get_comment_sum(self, obj):
        return Comment.objects.filter(bloc=obj).aggregate(Count('bloc'))

    def get_link_sum(self, obj):
        return Forvard_link.objects.filter(bloc=obj).aggregate(Count('bloc'))

    def get_sum_save(self, obj):
        return UserSave.objects.filter(bloc=obj).aggregate(Count('bloc'))

    def get_like(self, obj):
        request = self.context["request"]
        user = request.user
        print(user)
        likee = Like.objects.filter(bloc=obj, user=user).first()
        if likee:
            return True
        else:
            return False


class CommentSerializer1(serializers.ModelSerializer):
    comment = serializers.SerializerMethodField()
    like_avg = serializers.SerializerMethodField()
    like = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = "__all__"

    def get_comment(self, obj):
        comment = Comment.objects.filter(reply_id=obj).all()
        return ReplyCommentSerializer(comment, many=True, ).data

    def get_like_avg(self, obj):
        return Like.objects.filter(comment=obj).aggregate(Count('comment'))

    def get_like(self, obj):
        request = self.context["request"]
        user = request.user
        print(user)
        likee = Like.objects.filter(user=user).first()
        if likee:
            return True
        else:
            return False


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = '__all__'


class ReplyCommentSerializer(serializers.ModelSerializer):
    like_avg = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = '__all__'

    def get_like_avg(self, obj):
        return Like.objects.filter(comment=obj).aggregate(Count('comment'))


# подписчики
class ToSerializer(serializers.ModelSerializer):
    from_user = serializers.SerializerMethodField()

    class Meta:
        model = Subscription
        fields = ['from_user']

    def get_from_user(self, obj):
        return UserSerializer(obj.from_user).data


# подписки
class FromSerializer(serializers.ModelSerializer):
    to_user = serializers.SerializerMethodField()

    class Meta:
        model = Subscription
        fields = ['to_user']

    def get_to_user(self, obj):
        return UserSerializer(obj.to_user).data


class LikeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ['likee']
