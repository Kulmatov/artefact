from django.contrib.gis.db.models.functions import GeometryDistance
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render, redirect
from django.views import View
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from artefact.api.serilizer import *
from artefact.models import *


# UP_DATE
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def update_user(request):
    full_name = request.POST.get("full_name")
    adress = request.POST.get("asdress")
    sex = request.POST.get("sex")
    was_born = request.POST.get("was_born")
    desc = request.POST.get("about_me")

    user = request.user
    print(user.id)
    if user:
        if 'avatar' in request.FILES:
            upload = request.FILES['avatar']
            fss = FileSystemStorage()
            file = fss.save(upload.name, upload)
            user.avatar = file
        user.name = full_name
        user.adress = adress
        user.sex = sex
        user.was_born = was_born
        user.desc = desc
        user.save()
        res = {
            "status": 1,
            "msg": "changed successfully"
        }
    else:
        res = {
            "status": 0,
            "error": "wrong changed"
        }
    return Response(res)


# add bloc
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def add_bloc(request):
    try:
        user = request.user
        name = request.POST['name']
        upload = request.FILES['bloc']
        longitude = request.POST['longitude']
        latitude = request.POST['latitude']
        fss = FileSystemStorage()
        file = fss.save(upload.name, upload)
        Bloc.objects.create(
            name=name,
            media=file,
            user=user,
            longitude=longitude,
            latitude=latitude
        ).save()
        print(user.id)
        res = {
            "status": 1,
            "msg": "aded successfully"
        }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "error": "wrong changed"
        }
        return Response(res)


#  foto videolar save qilish
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def save_bloc(request):
    try:
        user = request.user
        bloc = request.data['bloc']
        user_save = UserSave.objects.filter(user=user, bloc_id=bloc).first()
        if not user_save:
            UserSave.objects.create(
                user=user,
                bloc_id=bloc
            ).save()
            res = {
                "status": 1,
                "msg": "Bloc append"
            }
        else:
            user_save.delete()
            res = {
                "status": 1,
                "msg": "Bloc canceled"
            }

    except KeyError:
        res = {
            "status": 0,
            "error": "key error"
        }
    return Response(res)


# save qilgan foto videolarni delete qilish
# @api_view(["POST"])
# @permission_classes([IsAuthenticated])
# def delete(request):
#     try:
#         user = request.user
#         bloc = request.data['bloc']
#         UserSave.objects.filter(user=user, bloc_id=bloc).delete()
#         res = {
#             "status": 1,
#             "msg": "Bloc deleted"
#         }
#
#     except KeyError:
#         res = {
#             "status": 0,
#             "msg": "don't deleted"
#         }
#
#     return Response(res)


# Like bosish
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def like(request):
    try:
        user = request.user
        bloc = request.data['bloc']
        # Bloc.objects.filter(user=user, bloc_id=bloc).exists()
        like = Like.objects.filter(user=user, bloc_id=bloc).first()
        if like:
            like.delete()
            res = {
                "status": 0,
                "msg": "bloc unliked",
                "sub": LikeeSerializer(many=False).data,
            }
        else:
            Like.objects.create(
                bloc_id=bloc,
                user=user,
                likee=True
            ).save()
            res = {
                "status": 1,
                "msg": "bloc liked",
                "sub": LikeeSerializer(Like, many=False).data,

            }
        return Response(res)

    except KeyError:
        res = {
            "status": 0,
            "msg": "error"
        }

    return Response(res)


# share
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def share(request):
    try:
        user = request.user
        bloc = request.data['bloc']
        bloc = Forvard_link.objects.filter(user=user, bloc_id=bloc).first()
        if not bloc:
            Forvard_link.objects.create(
                bloc_id=bloc,
                user=user
            ).save()
        res = {
            "status": 1,
            "msg": "Bloc shared"
        }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "msg": "error"
        }

    return Response(res)


# subscrib unsubscrib
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def subuser(request):
    try:
        user = request.user
        to_user_id = request.data['to_user_id']
        # to_user = User.objects.filter(id=to_user_id).first()
        subuser = Subscription.objects.filter(from_user=user, to_user_id=to_user_id).first()
        print(request.user.id)
        if subuser:
            subuser.delete()
            res = {
                "status": 0,
                "msg": "you are unsubscribed from user " + str(to_user_id)
            }
        else:
            Subscription.objects.create(
                from_user=user,
                to_user_id=to_user_id
            ).save()
            res = {
                "status": 1,
                "msg": "you are subscribed to user " + str(to_user_id)
            }
        return Response(res)

    except KeyError:
        res = {
            "status": 0,
            "msg": "error"
        }

    return Response(res)


# qaysi userni kuzatishi
@api_view(["GET"])
@permission_classes([IsAuthenticated])
def subuser_list(request):
    try:
        user = request.user
        print(user.id)
        subuser = Subscription.objects.filter(from_user=user).all()
        res = {
            "status": 1,
            "msg": "you are unsubscribed from ",
            "sub": ToSerializer(subuser, many=True).data,
        }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "msg": "error"
        }
    return Response(res)


# qaysi user uni kuzatishi
@api_view(["GET"])
@permission_classes([IsAuthenticated])
def folower_list(request):
    try:
        user = request.user
        print(user.id)
        subuser = Subscription.objects.filter(to_user=user).all()
        res = {
            "status": 1,
            "msg": "you are unsubscribed from ",
            "sub": FromSerializer(subuser, many=True).data,
        }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "msg": "error"
        }
    return Response(res)


# Create and reply comment
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def comment(request):
    try:
        user = request.user
        bloc = request.data['bloc']
        comment = request.POST['comment']
        reply = request.POST.get('reply')
        if bloc:
            Comment.objects.create(
                comment=comment,
                user=user,
                bloc_id=bloc,
                reply_id=reply

            ).save()
        res = {
            "status": 1,
            "msg": "Comment added"
        }
        return Response(res)
    except KeyError:
        res = {
            "status": 0,
            "msg": "error"
        }

    return Response(res)


# comment Like bosish
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def comment_like(request):
    try:
        user = request.user
        bloc = request.data['bloc']
        comment = request.POST.get('comment')
        # Bloc.objects.filter(user=user, bloc_id=bloc).exists()
        like = Like.objects.filter(user=user, bloc_id=bloc, comment_id=comment).first()
        if like:
            like.delete()
            res = {
                "status": 0,
                "msg": "bloc unliked",
                "sub": LikeeSerializer(many=False).data,
            }
        else:
            Like.objects.create(
                bloc_id=bloc,
                user=user,
                comment_id=comment,
            ).save()
            res = {
                "status": 1,
                "msg": "bloc liked",
                "sub": LikeeSerializer(Like, many=False).data,
            }
        return Response(res)

    except KeyError:
        res = {
            "status": 0,
            "msg": "error"
        }

    return Response(res)


# receive coordinate (принимать кординаты)
# @api_view(["POST"])
# @permission_classes([IsAuthenticated])
# def receive_cordinate(request):
#     user = request.user
#     latitude1 = request.data["latitude1"]
#     longitude1 = request.data["longitude1"]
#     ref_location = Point(float(latitude1), float(longitude1), srid=4326)
#     print(user.id)
#     ids = list(Subscription.objects.filter(from_user=user).values_list("to_user_id", flat=True))
#     print(ids)
#     qs = Bloc.objects.filter(user__in=ids)
#     qs = qs.annotate(distance=GeometryDistance("point", ref_location)).order_by(
#         '-distance')  # (-)bu kichikdan kattaga qarab sort
#
#     return Response({"status": 1, "data": BlocSerializer(qs, many=True, context={'request': request}).data})






