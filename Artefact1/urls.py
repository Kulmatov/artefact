"""Artefact1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers
from artefact.api.account_views import *
from artefact.api.api_views import *
from artefact.api.viewset import *

router = routers.DefaultRouter()
router.register(r'user', UserViewSet)
router.register(r'home', BlocViewset, basename=Bloc)
router.register(r'only_comment', CommentViewset)
router.register(r'usersave', UserSaveViewSet)
router.register(r'userbloc', UserBlocViewSet)
router.register(r'from', FromViewSet)
router.register(r'to', ToViewSet)

urlpatterns = [
                  path('api/v1/', include(router.urls)),
                  path('admin/', admin.site.urls),
                  path('api/v1/register/', save_register),
                  path('api/v1/sign_up/', login_view),
                  path('api/v1/add_username/', create_username),
                  path('api/v1/forgot/', forgot_password),
                  path('api/v1/accept/', accept_confirmation),
                  path('api/v1/new_pass/', new_password),
                  path('api/v1/update_user/', update_user),
                  path('api/v1/comment_like/', comment_like),
                  path('api/v1/add_bloc/', add_bloc),
                  path('api/v1/save_bloc/', save_bloc),
                  path('api/v1/like/', like),
                  path('api/v1/share/', share),
                  path('api/v1/subuser/', subuser),
                  path('api/v1/sublist/', subuser_list),
                  path('api/v1/folower_list/', folower_list),
                  path('api/v1/create_comment/', comment),

                  # path('api/v1/cordinate/', receive_cordinate),

              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
